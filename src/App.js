import React, { Component } from "react";
import Navbar from "./components/Navbar/Navbar";
import Users from "./pages/Users";
import Settings from "./pages/Settings";
import Loader from "./components/Loader/Loader";
import Message from "./components/Message/Message";
import axios from "axios";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import handleScroll from "./utilities/documentEvents";
import { nationalities } from "./consts";
import {
  newStateAfterUpdateUsers,
  newStateAfterNationalitySelect
} from "./utilities/state";
import { getUsers } from "./utilities/users";

/**
 * Main component
 *
 * @component
 * @example
 * return (
 *   <App />
 * )
 */
class App extends Component {
  constructor() {
    super();

    this.state = {
      searchTerm: "",
      isLoading: false,
      isError: false,
      endOfUsersCatalog: false,
      pageNumber: 1,
      results: [],
      selectedUser: {},
      nationalities: nationalities
    };
  }

  onSearch = searchTerm => {
    this.setState({ searchTerm });
  };

  getUsers = () => {
    const { isLoading, endOfUsersCatalog } = this.state;

    if (isLoading || endOfUsersCatalog) {
      return;
    }

    this.setState({
      isLoading: true
    });

    getUsers({
      pageNumber: this.state.pageNumber,
      nationalities: this.state.nationalities,
      onSucces: this.updateUsers,
      onError: this.setError,
      cancelToken: this.axiosCancelSource.token
    });
  };

  setError = () => {
    this.setState({ isError: true });
  };

  updateUsers = users => {
    this.setState(previousState =>
      newStateAfterUpdateUsers(previousState, users)
    );
  };

  onNationalitySelect = (checked, country) => {
    this.setState(
      previousState =>
        newStateAfterNationalitySelect(previousState, checked, country),
      this.getUsers
    );
  };

  componentDidMount() {
    window.addEventListener(
      "scroll",
      handleScroll(document.documentElement, this.getUsers)
    );
    this.axiosCancelSource = axios.CancelToken.source();

    this.getUsers();
  }

  componentWillUnmount() {
    window.removeEventListener(
      "scroll",
      handleScroll(document.documentElement, this.getUsers)
    );
    this.axiosCancelSource.cancel("Component unmounted.");
  }

  render() {
    const {
      results,
      searchTerm,
      isLoading,
      isError,
      endOfUsersCatalog
    } = this.state;

    return (
      <Router>
        <div className="App">
          <Navbar onSearch={this.onSearch} />

          <Switch>
            <Route path="/settings">
              <Settings
                nationalities={this.state.nationalities}
                onNationalitySelect={this.onNationalitySelect}
              />
            </Route>
            <Route path="/">
              <Users results={results} searchTerm={searchTerm} />

              {isLoading ? <Loader /> : null}

              {isError ? (
                <Message>General error. Please try to refresh page.</Message>
              ) : null}

              {endOfUsersCatalog ? (
                <Message>End of users catalog</Message>
              ) : null}
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
