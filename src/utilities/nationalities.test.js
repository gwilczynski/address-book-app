import { activeNationalities } from "./nationalities";

describe("nationalities", () => {
  describe("getActiveNationalities", () => {
    it("get active nationalities", () => {
      expect(
        activeNationalities({
          CH: true,
          ES: true,
          FR: true,
          GB: true
        })
      ).toEqual(["CH", "ES", "FR", "GB"]);

      expect(
        activeNationalities({
          CH: true,
          ES: false,
          FR: false,
          GB: true
        })
      ).toEqual(["CH", "GB"]);

      expect(
        activeNationalities({
          CH: false,
          ES: false,
          FR: false,
          GB: false
        })
      ).toEqual([]);
    });
  });
});
