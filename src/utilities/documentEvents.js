/**
 * attouch onscroll event
 * @param   {object} documentElement  document.documentElement
 * @param   {func} callback           what should have done is end of page is reached
 * @return  {undefined}
 */
const handleScroll = (documentElement, callback) => {
  window.onscroll = () => {
    const isEndOfPage =
      documentElement.scrollHeight - documentElement.scrollTop ===
      documentElement.clientHeight;

    if (isEndOfPage) {
      callback();
    }
  };
};

export default handleScroll;
