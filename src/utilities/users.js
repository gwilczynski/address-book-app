import axios from "axios";
import getResultsUrl from "./url";
import { nextBatchSizeOfUsers } from "../consts";
import { activeNationalities } from "./nationalities";

/**
 * Gets users for third party provider
 * @param   {int} pageNumber  Page number
 * @param   {array} nationalities  Nationalities
 * @param   {func} onSucces Wnat should run if resuls
 * @param   {func} onError  What should run if error occurs
 * @param   {string} cancelToken  Cancel token
 * @return  {string}
 */
const getUsers = ({
  pageNumber,
  nationalities,
  onSucces,
  onError,
  cancelToken
}) => {
  const url = getResultsUrl({
    pageNumber: pageNumber,
    resultsNumber: nextBatchSizeOfUsers,
    nationalities: activeNationalities(nationalities)
  });

  return axios
    .get(url, {
      cancelToken: cancelToken
    })
    .then(result => {
      onSucces(result.data.results);
    })
    .catch(error => {
      onError(error);
    });
};

/**
 * Search users based on first name and last name
 * @param   {array} users Users
 * @param   {string} searchTerm Search term
 * @return  {array}
 */
const filterUsers = (users, searchTerm) => {
  const searchTermLowerCased = searchTerm.toLowerCase();

  return users.filter(user =>
    `${user.name.first.toLowerCase()} ${user.name.last.toLowerCase()} `.startsWith(
      searchTermLowerCased
    )
  );
};

export { getUsers, filterUsers };
