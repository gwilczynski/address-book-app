/**
 * Creates usrl for third party provider
 * @param   {int} pageNumber  Page number
 * @param   {int} resultsNumber  Results number
 * @param   {array} nationalities  Nationalities
 * @return  {string}
 */
const getResultsUrl = ({ pageNumber, resultsNumber, nationalities }) => {
  if (!pageNumber || !resultsNumber || !nationalities) {
    throw Error(
      "Please provide all necessary data: pageNumber, resultsNumber, nationalities"
    );
  }

  return `https://randomuser.me/api/?nat=${nationalities.join(
    ","
  )}&page=${pageNumber}&results=${resultsNumber}`;
};

export default getResultsUrl;
