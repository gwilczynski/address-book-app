import handleScroll from "./documentEvents";

describe("documentEvents", () => {
  describe("handleScroll", () => {
    it("call callback", () => {
      const callback = jest.fn();
      const documentElement = {
        scrollHeight: 15,
        scrollTop: 5,
        clientHeight: 10
      };

      handleScroll(documentElement, callback);
      window.onscroll();
      expect(callback).toBeCalled();
    });

    it("does not call callback", () => {
      const callback = jest.fn();
      const documentElement = {
        scrollHeight: 15,
        scrollTop: 4,
        clientHeight: 10
      };

      handleScroll(documentElement, callback);
      window.onscroll();
      expect(callback).not.toBeCalled();
    });
  });
});
