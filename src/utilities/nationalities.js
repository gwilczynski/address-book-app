/**
 * Converts nationalities object to array
 * @param   {object} nationalities  Object contains nationalities
 * @return  {array}
 */
const nationalitiesToArray = nationalities => {
  return Object.keys(nationalities);
};

/**
 * Filters active nationalities
 * @param   {object} nationalities  Object contains nationalities
 * @return  {array} active nationalities
 */
const activeNationalities = nationalities => {
  return nationalitiesToArray(nationalities).filter(q => nationalities[q]);
};

export { activeNationalities, nationalitiesToArray };
