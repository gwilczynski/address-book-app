import axios from "axios";
import { getUsers, filterUsers } from "./users";

jest.mock("axios");

const onSucces = jest.fn();
const onError = jest.fn();

describe("users", () => {
  describe("filterUsers", () => {
    it("gets filter users", () => {
      const users = [
        {
          name: {
            first: "John",
            last: "Doe"
          }
        },
        {
          name: {
            first: "John",
            last: "Rambo"
          }
        },
        {
          name: {
            first: "Mickey",
            last: "Mouse"
          }
        }
      ];

      expect(filterUsers(users, "John")).toEqual([
        {
          name: {
            first: "John",
            last: "Doe"
          }
        },
        {
          name: {
            first: "John",
            last: "Rambo"
          }
        }
      ]);
      expect(filterUsers(users, "john rambo")).toEqual([
        {
          name: {
            first: "John",
            last: "Rambo"
          }
        }
      ]);
      expect(filterUsers(users, "John Rambo")).toEqual([
        {
          name: {
            first: "John",
            last: "Rambo"
          }
        }
      ]);
      expect(filterUsers(users, "Superman")).toEqual([]);
    });
  });

  describe("getUsers", () => {
    const request = {
      pageNumber: 1,
      nationalities: ["fr", "de", "pl"],
      cancelToken: "cancelToken"
    };

    beforeEach(() => {
      jest.clearAllMocks();
    });

    it("calls onSucces", async () => {
      axios.get.mockResolvedValue({ data: { results: ["bar"] } });

      await getUsers({ ...request, onSucces, onError });

      expect(
        axios.get
      ).toHaveBeenCalledWith(
        "https://randomuser.me/api/?nat=0,1,2&page=1&results=50",
        { cancelToken: "cancelToken" }
      );
      expect(onSucces).toHaveBeenCalledWith(["bar"]);
      expect(onError).not.toHaveBeenCalled();
    });

    it("calls onError", async () => {
      const errorMessage = "Network Error";

      axios.get.mockRejectedValue(errorMessage);

      await getUsers({ ...request, onSucces, onError });

      expect(onSucces).not.toHaveBeenCalled();
      expect(onError).toHaveBeenCalledWith(errorMessage);
    });
  });
});
