import getResultsUrl from "./url";

describe("url", () => {
  describe("getResultsUrl", () => {
    it("throws an exception", () => {
      const error = new Error(
        "Please provide all necessary data: pageNumber, resultsNumber, nationalities"
      );

      expect(() => {
        getResultsUrl({
          pageNumber: null,
          resultsNumber: 12,
          nationalities: ["de", "fr"]
        });
      }).toThrowError(error);

      expect(() => {
        getResultsUrl({
          pageNumber: 1,
          resultsNumber: null,
          nationalities: ["de", "fr"]
        });
      }).toThrowError(error);

      expect(() => {
        getResultsUrl({
          pageNumber: 1,
          resultsNumber: 2,
          nationalities: null
        });
      }).toThrowError(error);
    });

    it("gets correct url", () => {
      const result = getResultsUrl({
        pageNumber: 1,
        resultsNumber: 50,
        nationalities: ["de", "fr"]
      });
      expect(result).toEqual(
        "https://randomuser.me/api/?nat=de,fr&page=1&results=50"
      );
    });

    it("gets correct url for all nationalities", () => {
      const result = getResultsUrl({
        pageNumber: 2,
        resultsNumber: 30,
        nationalities: []
      });
      expect(result).toEqual(
        "https://randomuser.me/api/?nat=&page=2&results=30"
      );
    });
  });
});
