import { maxLengthOfTheCatalogue } from "../consts";

/**
 * Creates new state after update users
 * @param   {object} previousState  previousState
 * @param   {array} users users
 * @return  {object}
 */
const newStateAfterUpdateUsers = (previousState, users) => {
  const mergedUsers = [...previousState.results, ...users];

  return {
    pageNumber: previousState.pageNumber + 1,
    results: mergedUsers,
    endOfUsersCatalog: mergedUsers.length >= maxLengthOfTheCatalogue,
    isLoading: false
  };
};

/**
 * Creates new state after nationality select
 * @param   {object} previousState  previousState
 * @param   {boolean} checked  is checked or not
 * @param   {string} country  two letters countryn code
 * @return  {object}
 */
const newStateAfterNationalitySelect = (previousState, checked, country) => {
  return {
    nationalities: { ...previousState.nationalities, [country]: checked },
    pageNumber: 1,
    endOfUsersCatalog: false,
    results: []
  };
};

export { newStateAfterUpdateUsers, newStateAfterNationalitySelect };
