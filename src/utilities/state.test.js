import {
  newStateAfterUpdateUsers,
  newStateAfterNationalitySelect
} from "./state";

jest.mock("../consts", () => ({
  maxLengthOfTheCatalogue: 3
}));

describe("state", () => {
  describe("newStateAfterUpdateUsers", () => {
    it("returns new state", () => {
      const previousState = {
        results: [],
        pageNumber: 1
      };
      const users = [
        {
          firstName: "John",
          lastName: "Rambo"
        },
        {
          firstName: "Bruce",
          lastName: "Wane"
        }
      ];

      expect(newStateAfterUpdateUsers(previousState, users)).toEqual({
        endOfUsersCatalog: false,
        isLoading: false,
        pageNumber: 2,
        results: [
          { firstName: "John", lastName: "Rambo" },
          { firstName: "Bruce", lastName: "Wane" }
        ]
      });
    });

    it("returns new state and endOfUsersCatalog", () => {
      const previousState = {
        results: [
          {
            firstName: "Clark",
            lastName: "Kent"
          }
        ],
        pageNumber: 2
      };
      const users = [
        {
          firstName: "John",
          lastName: "Rambo"
        },
        {
          firstName: "Bruce",
          lastName: "Wane"
        }
      ];

      expect(newStateAfterUpdateUsers(previousState, users)).toEqual({
        endOfUsersCatalog: true,
        isLoading: false,
        pageNumber: 3,
        results: [
          {
            firstName: "Clark",
            lastName: "Kent"
          },
          { firstName: "John", lastName: "Rambo" },
          { firstName: "Bruce", lastName: "Wane" }
        ]
      });
    });
  });

  describe("newStateAfterNationalitySelect", () => {
    it("returns new state for exisitng country", () => {
      const previousState = {
        nationalities: {
          de: false,
          fr: false
        }
      };
      const checked = true;
      const country = "fr";

      expect(
        newStateAfterNationalitySelect(previousState, checked, country)
      ).toEqual({
        nationalities: { de: false, fr: true },
        pageNumber: 1,
        endOfUsersCatalog: false,
        results: []
      });
    });

    it("returns new state for new country", () => {
      const previousState = {
        nationalities: {
          de: false,
          fr: false
        }
      };
      const checked = true;
      const country = "pl";

      expect(
        newStateAfterNationalitySelect(previousState, checked, country)
      ).toEqual({
        nationalities: { de: false, fr: false, pl: true },
        pageNumber: 1,
        endOfUsersCatalog: false,
        results: []
      });
    });
  });
});
