import React from "react";
import Settings from "./Settings";
import { render, fireEvent, cleanup } from "@testing-library/react";

describe("Settings", () => {
  const onNationalitySelect = jest.fn();

  afterEach(() => {
    cleanup();
  });

  const nationalities = { pl: true, es: false };

  it("gets two rows with nationalities", () => {
    const { queryByTestId } = render(
      <Settings
        nationalities={nationalities}
        onNationalitySelect={onNationalitySelect}
      />
    );

    expect(queryByTestId("nationality-0").textContent).toEqual("pl");
    expect(queryByTestId("nationality-1").textContent).toEqual("es");

    expect(queryByTestId("checkbox-0").checked).toEqual(true);
    expect(queryByTestId("checkbox-1").checked).toEqual(false);
  });

  it("fires event when checked", () => {
    const { queryByTestId } = render(
      <Settings
        nationalities={nationalities}
        onNationalitySelect={onNationalitySelect}
      />
    );

    fireEvent.click(queryByTestId("checkbox-0"));
    fireEvent.click(queryByTestId("checkbox-1"));

    expect(onNationalitySelect).toHaveBeenCalledTimes(2);
    expect(onNationalitySelect).toHaveBeenCalledWith(false, "pl");
    expect(onNationalitySelect).toHaveBeenCalledWith(true, "es");
  });
});
