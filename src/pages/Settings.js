import React from "react";
import { nationalitiesToArray } from "../utilities/nationalities";
import PropTypes from "prop-types";

/**
 * Component contains info about selected nationalities of users
 *
 * @component
 *
 * @prop {object} nationalities - available nationalities
 * @prop {func} onNationalitySelect - this function it triggered you enable/disable nationality
 *
 * @example
 * const nationalities = { pl: true, es: false };
 * const onNationalitySelect = () => '';
 * return (
 *   <Settings nationalities={nationalities} onNationalitySelect={onNationalitySelect} />
 * )
 */
const Settings = props => {
  const { nationalities, onNationalitySelect } = props;

  return (
    <main role="main" className="container">
      <div className="jumbotron jumbotron-fluid">
        <div className="container">
          <h1 className="display-4">Settings</h1>
        </div>
      </div>

      <div className="content">
        <table className="table">
          <thead>
            <tr>
              <th scope="col"></th>
              <th scope="col">Nationality</th>
            </tr>
          </thead>
          <tbody>
            {nationalitiesToArray(nationalities).map((nationality, idx) => (
              <tr key={idx}>
                <th scope="row">
                  <input
                    type="checkbox"
                    aria-label="Checkbox for following text input"
                    data-testid={`checkbox-${idx}`}
                    onChange={event =>
                      onNationalitySelect(event.target.checked, nationality)
                    }
                    checked={nationalities[nationality]}
                  />
                </th>
                <td data-testid={`nationality-${idx}`}>{nationality}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </main>
  );
};

Settings.propTypes = {
  nationalities: PropTypes.object.isRequired,
  onNationalitySelect: PropTypes.func.isRequired
};

export default Settings;
