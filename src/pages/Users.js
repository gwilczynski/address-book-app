import React, { useState } from "react";
import Card from "../components/Card/Card";
import CardModal from "../components/CardModal/CardModal";
import { filterUsers } from "../utilities/users";
import PropTypes from "prop-types";

/**
 * Component renders user list as grid
 *
 * @component
 *
 * @prop {object} results - results from third party provider
 * @prop {func} searchTerm - search term
 *
 * @example
 * const results = [
 *   {
 *     name: {
 *       first: "John",
 *       last: "Doe"
 *     },
 *     location: {
 *       street: { name: "street name" },
 *       city: "city",
 *       state: "state",
 *       postcode: "postcode"
 *     },
 *     login: {
 *       username: "john_doe",
 *       uuid: "q1w2e3"
 *     },
 *     picture: { thumbnail: "" }
 *   },
 *   {
 *     name: {
 *       first: "John",
 *       last: "Rambo"
 *     },
 *     login: {
 *       username: "john_rambo",
 *       uuid: "r4t5y6"
 *     },
 *     picture: { thumbnail: "" }
 *   },
 *   {
 *     name: {
 *       first: "Mickey",
 *       last: "Mouse"
 *     },
 *     login: {
 *       username: "mickey_mouse",
 *       uuid: "u7i8o9"
 *     },
 *     picture: { thumbnail: "" }
 *   }
 * ];
 * const searchTerm = 'John';
 * return (
 *   <Users results={results} searchTerm={searchTerm} />
 * )
 */
const Users = props => {
  const { results, searchTerm } = props;
  const [selectedUser, setSelectedUser] = useState(null);

  if (!results || !results.length) {
    return null;
  }

  return (
    <main role="main" className="container">
      <div className="jumbotron jumbotron-fluid">
        <div className="container">
          <h1 className="display-4">Contacts</h1>
        </div>
      </div>

      <CardModal
        selectedUser={selectedUser}
        onClose={() => setSelectedUser(null)}
      />

      <div className="container">
        <div className="row">
          {filterUsers(results, searchTerm).map(result => {
            return (
              <Card
                result={result}
                key={result.login.uuid}
                showDetailsModal={setSelectedUser}
              />
            );
          })}
        </div>
      </div>
    </main>
  );
};

Users.propTypes = {
  results: PropTypes.array.isRequired,
  searchTerm: PropTypes.string.isRequired
};

export default Users;
