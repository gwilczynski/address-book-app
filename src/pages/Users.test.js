import React from "react";
import Users from "./Users";
import { render, fireEvent, cleanup } from "@testing-library/react";

describe("Settings", () => {
  afterEach(() => {
    cleanup();
  });

  const results = [
    {
      name: {
        first: "John",
        last: "Doe"
      },
      location: {
        street: { name: "street name" },
        city: "city",
        state: "state",
        postcode: "postcode"
      },
      login: {
        username: "john_doe",
        uuid: "q1w2e3"
      },
      picture: { thumbnail: "" }
    },
    {
      name: {
        first: "John",
        last: "Rambo"
      },
      login: {
        username: "john_rambo",
        uuid: "r4t5y6"
      },
      picture: { thumbnail: "" }
    },
    {
      name: {
        first: "Mickey",
        last: "Mouse"
      },
      login: {
        username: "mickey_mouse",
        uuid: "u7i8o9"
      },
      picture: { thumbnail: "" }
    }
  ];

  it("gets three cards", () => {
    const searchTerm = "";
    const { getAllByTestId } = render(
      <Users results={results} searchTerm={searchTerm} />
    );
    expect(getAllByTestId("card-title")[0].textContent).toEqual("John Doe");
    expect(getAllByTestId("card-title")[1].textContent).toEqual("John Rambo");
    expect(getAllByTestId("card-title")[2].textContent).toEqual("Mickey Mouse");
  });

  it("gets one card", () => {
    const searchTerm = "John Rambo";
    const { getAllByTestId } = render(
      <Users results={results} searchTerm={searchTerm} />
    );
    expect(getAllByTestId("card-title")[0].textContent).toEqual("John Rambo");
  });

  it("gets no card", () => {
    const searchTerm = "John Rambo";
    const { queryAllByTestId } = render(
      <Users results={[]} searchTerm={searchTerm} />
    );
    expect(queryAllByTestId("card-title")).toEqual([]);
  });

  it("opens and close modal", () => {
    const { queryByTestId } = render(
      <Users results={results} searchTerm={""} />
    );

    fireEvent.click(queryByTestId("details-button-q1w2e3"));
    expect(queryByTestId("modal-title").textContent).toEqual("Contact details");
    fireEvent.click(queryByTestId("close-modal"));
    expect(queryByTestId("modal-title")).toEqual(null);
  });
});
