/**
 * return full list of available nationalities
 * @return {object} full list of available nationalities
 */
const nationalities = {
  CH: true,
  ES: true,
  FR: true,
  GB: true
};

/**
 * Max length of the catalogue
 * @return {int} Max length of the catalogue
 */
const maxLengthOfTheCatalogue = 1000;

/**
 * Next batch size of users
 * @return {int} Next batch size of users
 */
const nextBatchSizeOfUsers = 50;

export { nationalities, maxLengthOfTheCatalogue, nextBatchSizeOfUsers };
