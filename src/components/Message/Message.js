import React from "react";
import PropTypes from "prop-types";

/**
 * Component shows message info
 *
 * @component
 *
 * @prop {string} children - contains message
 *
 * @example
 * return (
 *   <Message>Put your info here</Message>
 * )
 */
const Message = props => {
  const { children } = props;

  return (
    <div className="d-flex align-items-center">
      <strong>{children}</strong>
    </div>
  );
};

Message.propTypes = {
  children: PropTypes.string.isRequired
};

export default Message;
