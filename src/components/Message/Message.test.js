import React from "react";
import Message from "./Message";
import { render, cleanup } from "@testing-library/react";

describe("Settings", () => {
  afterEach(() => {
    cleanup();
  });

  it("gets two rows with nationalities", () => {
    const { queryByText } = render(<Message>Some message</Message>);

    expect(queryByText("Some message")).not.toBe(null);
  });
});
