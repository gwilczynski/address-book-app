import React from "react";
import Message from "../Message/Message";

/**
 * Component shows loading process
 *
 * @component
 * @example
 * return (
 *   <Loader />
 * )
 */
const Loader = () => {
  return <Message>Loading...</Message>;
};

export default Loader;
