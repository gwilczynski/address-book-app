import React from "react";
import Loader from "./Loader";
import { render, cleanup } from "@testing-library/react";

describe("Settings", () => {
  afterEach(() => {
    cleanup();
  });

  it("gets two rows with nationalities", () => {
    const { queryByText } = render(<Loader />);

    expect(queryByText("Loading...")).not.toBe(null);
  });
});
