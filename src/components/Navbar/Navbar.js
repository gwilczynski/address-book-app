import React from "react";
import Search from "../Search/Search";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

/**
 * Component contains lins to contact page and settings page
 *
 * @component
 *
 * @prop {string} onSearch - this function it triggered when serach input is changed
 *
 * @example
 * const onSearch = () => '';
 * return (
 *   <Navbar onSearch={onSearch} />
 * )
 */
const Navbar = props => {
  const { onSearch } = props;

  return (
    <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <div className="collapse navbar-collapse" id="navbarCollapse">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to="/" className="nav-link">
              Contacts
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/settings" className="nav-link">
              Settings
            </Link>
          </li>
        </ul>
        <Search onChange={onSearch} />
      </div>
    </nav>
  );
};

Navbar.propTypes = {
  onSearch: PropTypes.func.isRequired
};

export default Navbar;
