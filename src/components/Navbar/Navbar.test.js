import React from "react";
import Navbar from "./Navbar";
import { render, fireEvent, cleanup } from "@testing-library/react";

jest.mock("react-router-dom", () => {
  return {
    Link: () => {
      return <div></div>;
    }
  };
});

describe("Navbar", () => {
  afterEach(() => {
    cleanup();
  });

  it("changes search term", () => {
    const onSearch = jest.fn();
    const { queryByTestId } = render(<Navbar onSearch={onSearch} />);

    fireEvent.change(queryByTestId("search-input"), {
      target: { value: "Bruce Lee" }
    });

    expect(onSearch).toHaveBeenCalledTimes(1);
    expect(onSearch).toHaveBeenCalledWith("bruce lee");
  });
});
