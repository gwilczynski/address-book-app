import React from "react";
import Card from "./Card";
import { render, fireEvent, cleanup } from "@testing-library/react";

const result = {
  name: {
    first: "John",
    last: "Rambo"
  },
  login: {
    username: "john_rambo",
    uuid: "r4t5y6"
  },
  picture: { thumbnail: "picture_thumbnail" },
  email: "gmail@john_rambo.com"
};

describe("Card", () => {
  const showDetailsModal = jest.fn();

  afterEach(() => {
    cleanup();
  });

  it("render card", () => {
    const { getByTestId } = render(
      <Card result={result} showDetailsModal={showDetailsModal} />
    );

    expect(getByTestId("card-img").getAttribute("alt")).toEqual("john_rambo");
    expect(getByTestId("card-img").getAttribute("src")).toEqual(
      "picture_thumbnail"
    );
    expect(getByTestId("card-title").textContent).toEqual("John Rambo");
    expect(getByTestId("card-username").textContent).toEqual("john_rambo");
    expect(getByTestId("card-email").textContent).toEqual(
      "gmail@john_rambo.com"
    );
  });

  it("clicks on button", () => {
    const { getByTestId } = render(
      <Card result={result} showDetailsModal={showDetailsModal} />
    );

    fireEvent.click(getByTestId("details-button-r4t5y6"));

    expect(showDetailsModal).toHaveBeenCalledWith(result);
  });
});
