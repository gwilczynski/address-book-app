import React from "react";
import PropTypes from "prop-types";

/**
 * Component showins user info.
 *
 * @component
 *
 * @prop {object} result - contains info about user
 * @prop {func} showDetailsModal - trigger card modal
 *
 * @example
 * const result = {
 *   name: {
 *    first: "John",
 *    last: "Rambo"
 *  },
 *  login: {
 *    username: "john_rambo",
 *    uuid: "r4t5y6"
 *  },
 *  picture: { thumbnail: "picture_thumbnail" },
 *  email: "gmail@john_rambo.com"
 * };
 * const showDetailsModal = () => '';
 * return (
 *   <Card result={result} showDetailsModal={showDetailsModal} />
 * )
 */
const Card = props => {
  const { result, showDetailsModal } = props;
  const { picture, name, login, email } = result;

  return (
    <div className="col-lg-3 py-3">
      <div className="card">
        <div className="card-body">
          <img
            alt={login.username}
            src={picture.thumbnail}
            data-testid="card-img"
          />
          <h5 className="card-title" data-testid="card-title">
            {name.first} {name.last}
          </h5>
          <p className="card-text" data-testid="card-username">
            {login.username}
          </p>
          <p className="card-text" data-testid="card-email">
            {email}
          </p>
          <button
            className="btn btn-primary"
            data-testid={`details-button-${login.uuid}`}
            onClick={() => showDetailsModal(result)}
          >
            Details
          </button>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  showDetailsModal: PropTypes.func.isRequired,
  result: PropTypes.object.isRequired
};

export default Card;
