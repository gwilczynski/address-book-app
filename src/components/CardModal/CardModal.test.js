import React from "react";
import CardModal from "./CardModal";
import { render, fireEvent, cleanup } from "@testing-library/react";

const selectedUser = {
  name: {
    first: "John",
    last: "Rambo"
  },
  login: {
    username: "john_rambo",
    uuid: "r4t5y6"
  },
  picture: { thumbnail: "picture_thumbnail" },
  email: "gmail@john_rambo.com",
  phone: "555-GHOSTBUSTERS",
  cell: "555-2368",
  location: {
    street: { name: "street name" },
    city: "city",
    state: "state",
    postcode: "postcode"
  }
};

describe("CardModal", () => {
  const onClose = jest.fn();

  afterEach(() => {
    cleanup();
  });

  it("renders empty card modal", () => {
    const { queryByTestId } = render(
      <CardModal selectedUser={null} onClose={onClose} />
    );

    expect(queryByTestId("card-modal")).toBe(null);
  });

  it("renders card modal", () => {
    const { getByText, getByTestId } = render(
      <CardModal selectedUser={selectedUser} onClose={onClose} />
    );

    expect(getByTestId("card-modal")).toBeDefined();
    expect(getByText("Street Name: street name")).toBeDefined();
    expect(getByText("City: city")).toBeDefined();
    expect(getByText("State: state")).toBeDefined();
    expect(getByText("Postcode: postcode")).toBeDefined();
    expect(getByText("Phone: 555-GHOSTBUSTERS")).toBeDefined();
    expect(getByText("Cell: 555-2368")).toBeDefined();
  });

  it("closes card modal", () => {
    const { getByTestId } = render(
      <CardModal selectedUser={selectedUser} onClose={onClose} />
    );

    const closeButton = getByTestId("close-modal");
    fireEvent.click(closeButton);

    expect(onClose).toHaveBeenCalled();
  });
});
