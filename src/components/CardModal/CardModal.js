import React from "react";
import PropTypes from "prop-types";

/**
 * Component showins detailed user info.
 *
 * @component
 *
 * @prop {object} selectedUser - contains detailed info about user
 * @prop {func} onClose - closes modal
 *
 * @example
 * const selectedUser = {
 *  name: {
 *    first: "John",
 *    last: "Rambo"
 *  },
 *  login: {
 *    username: "john_rambo",
 *    uuid: "r4t5y6"
 *  },
 *  picture: { thumbnail: "picture_thumbnail" },
 *  email: "gmail@john_rambo.com",
 *  phone: "555-GHOSTBUSTERS",
 *  cell: "555-2368",
 *  location: {
 *    street: { name: "street name" },
 *    city: "city",
 *    state: "state",
 *    postcode: "postcode"
 * };
 * const onClose = () => '';
 * return (
 *   <CardModal selectedUser={selectedUser} onClose={onClose} />
 * )
 */
const CardModal = props => {
  const { selectedUser, onClose } = props;

  if (!selectedUser) {
    return <div></div>;
  }

  return (
    <div>
      <div
        className="modal fade show"
        tabIndex="-1"
        role="dialog"
        style={{ display: "block" }}
        aria-modal="true"
        aria-labelledby="exampleModalLiveLabel"
        data-testid="card-modal"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" data-testid="modal-title">
                Contact details
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                data-testid="close-modal"
                onClick={onClose}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <p>Street Name: {selectedUser.location.street.name}</p>
              <p>City: {selectedUser.location.city}</p>
              <p>State: {selectedUser.location.state}</p>
              <p>Postcode: {selectedUser.location.postcode}</p>
              <p>Phone: {selectedUser.phone}</p>
              <p>Cell: {selectedUser.cell}</p>
            </div>
          </div>
        </div>
      </div>
      <div className="modal-backdrop fade show"></div>
    </div>
  );
};

CardModal.propTypes = {
  selectedUser: PropTypes.object,
  onClose: PropTypes.func.isRequired
};

export default CardModal;
