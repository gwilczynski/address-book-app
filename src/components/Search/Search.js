import React from "react";
import PropTypes from "prop-types";

/**
 * Component contains lins to contact page and settings page
 *
 * @component
 *
 * @prop {func} onChange - this function it triggered when serach input is changed
 *
 * @example
 * const onChange = () => '';
 * return (
 *   <Search onChange={onChange} />
 * )
 */
const Search = props => {
  const { onChange } = props;

  return (
    <form className="form-inline mt-2 mt-md-0">
      <input
        className="form-control mr-sm-2"
        type="text"
        placeholder="Search"
        aria-label="Search"
        data-testid="search-input"
        onChange={event => onChange(event.target.value.toLowerCase())}
      />
    </form>
  );
};

Search.propTypes = {
  onChange: PropTypes.func.isRequired
};

export default Search;
