import React from "react";
import Search from "./Search";
import { render, fireEvent, cleanup } from "@testing-library/react";

describe("Search", () => {
  afterEach(() => {
    cleanup();
  });

  it("changes search term", () => {
    const onChange = jest.fn();
    const { queryByTestId } = render(<Search onChange={onChange} />);

    fireEvent.change(queryByTestId("search-input"), {
      target: { value: "Bruce Lee" }
    });

    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith("bruce lee");
  });
});
