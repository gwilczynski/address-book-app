# Address Book App

This is a fictional address book app, where you can search for users addresses and personal information. The app displays a list of users for clients to browse and get personal information for a selected user. This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Start project

To run the app in the development mode.

```bash
$ yarn install && yarn start
```


## Run tests 

Launch the test runner in the interactive watch mode:

```bash
yarn test
```

Launch the test runner in the interactive watch mode with test coverage:

```bash
yarn test --watchAll --coverage
```

## Build project

Build the app for production to the `build` folder.

```
yarn build
```

## Build documentation

Build documentation to the `docs` folder.

```
yarn docs
```
